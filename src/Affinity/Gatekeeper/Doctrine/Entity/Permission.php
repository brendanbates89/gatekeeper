<?php
/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Doctrine\Entity;

use Affinity\Gatekeeper\Model\PermissionInterface;

/**
 * This is a generic version of an annotated Permission entity, compatible
 * with the Doctrine 2 ORM package.
 * 
 * @package Affinity.Gatekeeper
 * 
 * @Entity 
 * @Table(name="Permissions")
 */
class Permission implements PermissionInterface
{
    /***************************
     * Entity Properties
     ***************************/
    
    /**
     * @Id 
     * @Column(type="integer") 
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(type="integer")
     */
    protected $RoleId;
    
    /**
     * @Column(type="integer")
     */
    protected $ResourceId;
    
    /**
     * @Column(type="string")
     */
    protected $ResourceKey;
    
    /**
     * @var array
     * 
     * @OneToMany(targetEntity="PermissionAction", mappedBy="permission")
     */
    protected $permissionActions;
    
    /**
     * @var Resource
     * 
     * @OneToOne(targetEntity="Resource")
     * @JoinColumn(name="ResourceId", referencedColumnName="Id")
     */
    protected $resource;
    
    /**
     * @var Role
     * 
     * @ManyToOne(targetEntity="Role", inversedBy="permissions")
     * @JoinColumn(name="RoleId", referencedColumnName="Id")
     */
    protected $role;
    
    
    /***************************
     * Entity Getters and Setters
     ***************************/
    
    public function getId()
    {
        return $this->Id;
    }
    
    public function getResourceName()
    {
        return $this->resource->getName();
    }
    
    public function setResourceName($name)
    {
        //$this->ResourceName = $name;
    }

    public function getActions()
    {
        $returnArray = array();
        
        foreach($this->permissionActions as $permissionAction)
        {
            $returnArray[] = $permissionAction->getAction();
        }
        
        return $returnArray;
    }

    public function setActions(array $actions)
    {
        //$this->actions = $actions;
    }

    public function getResourceKey()
    {
        return $this->ResourceKey;
    }
    
    public function setResourceKey($key)
    {
        //$this->ResourceKey = $key;
    }
}