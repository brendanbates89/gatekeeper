<?php
/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Doctrine\Entity;

use Affinity\Gatekeeper\Model\ActionInterface;

/**
 * This is a generic version of an annotated Action entity, compatible
 * with the Doctrine 2 ORM package.
 * 
 * @package Affinity.Gatekeeper
 * 
 * @Entity 
 * @Table(name="Actions")
 */
class Action implements ActionInterface
{
    /***************************
     * Entity Properties
     ***************************/
    
    /**
     * @Id
     * @Column(type="integer") 
     * @GeneratedValue
     */
    protected $Id;
        
    /**
     * @Column(type="string")
     */
    protected $Name;
    
    protected $value;
    
    /***************************
     * Entity Getters and Setters
     ***************************/
    
    public function getId()
    {
        return $this->Id;
    }

    public function getName()
    {
        return $this->Name;
    }
    
    public function setName($name)
    {
        $this->Name = $name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }
    
    public function getPermission()
    {
        //return $this->permission;
    }
}