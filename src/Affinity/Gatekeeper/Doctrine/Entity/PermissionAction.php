<?php
/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Doctrine\Entity;

/**
 * This is a generic version of an annotated Permission entity, compatible
 * with the Doctrine 2 ORM package.
 * 
 * @package Affinity.Gatekeeper
 * 
 * @Entity 
 * @Table(name="PermissionActions")
 */
class PermissionAction
{
    /***************************
     * Entity Properties
     ***************************/
    
    /**
     * @Id 
     * @Column(type="integer") 
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(type="integer")
     */
    protected $PermissionId;
    
    /**
     * @Column(type="integer")
     */
    protected $ActionId;
    
    /**
     * @Column(type="integer")
     */
    protected $Value;
    
    /**
     * @ManyToOne(targetEntity="Permission", inversedBy="permissionActions")
     * @JoinColumn(name="PermissionId", referencedColumnName="Id")
     */
    protected $permission;
    
    /**
     * @var Action
     * 
     * @OneToOne(targetEntity="Action")
     * @JoinColumn(name="ActionId", referencedColumnName="Id")
     */
    protected $action;
    
    
    /***************************
     * Entity Getters and Setters
     ***************************/
    
    public function getId()
    {
        return $this->Id;
    }
    
    public function getAction()
    {
        $this->action->setValue($this->Value);
        return $this->action;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function getPermission()
    {
        return $this->permission;
    }
    
    public function setPermission($permission)
    {
        $this->permission = $permission;
    }
}