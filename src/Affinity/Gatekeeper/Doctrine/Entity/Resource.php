<?php
/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Doctrine\Entity;

/**
 * This is a generic version of an annotated Permission entity, compatible
 * with the Doctrine 2 ORM package.
 * 
 * @package Affinity.Gatekeeper
 * 
 * @Entity 
 * @Table(name="Resources")
 */
class Resource
{
    /***************************
     * Entity Properties
     ***************************/
    
    /**
     * @Id 
     * @Column(type="integer") 
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(type="text")
     */
    protected $Name;    
    
    /***************************
     * Entity Getters and Setters
     ***************************/
    
    public function getId()
    {
        return $this->Id;
    }
    
    public function getName()
    {
        return $this->Name;
    }
    
    public function setName($name)
    {
        $this->Name = $name;
    }
}