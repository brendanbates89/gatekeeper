<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Model;

use Affinity\Gatekeeper\Model\RoleInterface;
use Affinity\Gatekeeper\Model\PermissionInterface;

/**
 * 
 * Describes the interface for a user.
 * 
 * @package Affinity.Gatekeeper
 * 
 */
interface UserInterface
{    
    /**
     * Returns an array of Roles.
     * 
     * @return RoleInterface[] Array of roles pertaining to the user.
     */
    public function getRoles();    
}
