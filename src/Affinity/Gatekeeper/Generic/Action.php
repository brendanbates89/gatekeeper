<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Generic;

use Affinity\Gatekeeper\Model\ActionInterface;

/**
 * 
 * Generic Action class to be used with Gatekeeper.
 * 
 * @package Affinity.Gatekeeper
 * 
 */
class Action implements ActionInterface
{
    /**
     * The string name of the action.
     * 
     * @var string $actionName
     */
    private $actionName;
    
    /**
     * The value of the action.
     * 
     * @var mixed $actionValue
     */
    private $actionValue;
    
    /**
     * Default action names to be used.  Note that any string can be
     * used as an action name, however it is best to always use constant
     * values defined in code to avoid discrepancies in the database.
     */
    const IsGranted = "Gatekeeper_IsGranted";
    const Create = "Gatekeeper_Create";
    const Read = "Gatekeeper_Read";
    const Update = "Gatekeeper_Update";
    const Delete = "Gatekeeper_Delete";
    const Undelete = "Gatekeeper_Undelete";
    
    /**
     * Default constructor, with name and value setters.
     * 
     * @param type $name
     * @param type $value
     */
    public function __construct($name = null, $value = null)
    {
        $this->setName($name);
        $this->setValue($value);
    }
    
    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->actionName;
    }

    /**
     * @inheritdoc
     */
    public function getValue() 
    {
        return $this->actionValue;
    }

    /**
     * @inheritdoc
     */
    public function setName($name) 
    {
        $this->actionName = $name;
    }

    /**
     * @inheritdoc
     */
    public function setValue($value) 
    {
        $this->actionValue = $value;
    }    
}
