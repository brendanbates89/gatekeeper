<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Generic;

use Affinity\Gatekeeper\Model\PermissionInterface;
use Affinity\Gatekeeper\Generic\Resource\ObjectResourceInterface;
use Affinity\Gatekeeper\Model\ActionInterface;

/**
 * 
 * Represents a permission.  A permission relates one or
 * more properties to a resource.
 *
 * @package Affinity.Gatekeeper
 * 
 */
class Permission implements PermissionInterface
{
    private $resourceName;
    private $resourceKey;
    private $actions = array();
    
    public function __construct($actions = null, $resourceName = null, $resourceKey = null)
    {
        if($resourceName instanceof ObjectResourceInterface)
        {
            $this->resourceName = $resourceName->getResourceName();
            $this->resourceKey = $resourceName->getResourceKey();
        } else
        {
            $this->resourceName = $resourceName;
            $this->resourceKey = $resourceKey;
        }
        
        if(is_array($actions))
            $this->actions = $actions;
        else if($actions instanceof Action)
            $this->actions = array($actions);
    }
    

    /**
     * @inheritdoc
     */
    public function getActions() 
    {
        return $this->actions;
    }
    
    /**
     * @inheritdoc
     */
    public function setActions(array $actions)
    {
        $this->actions = $actions;
    }
    
    /**
     * 
     */
    public function addAction(ActionInterface $action)
    {
        $this->actions[] = $action;
    }

    /**
     * @inheritdoc
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }
    
    /**
     * @inheritdoc
     */
    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;
    }
    
    /**
     * @inheritdoc
     */
    public function getResourceKey()
    {
        return $this->resourceKey;
    }
    
    /**
     * @inheritdoc
     */
    public function setResourceKey($resourceKey)
    {
        $this->resourceKey = $resourceKey;
    }
}
