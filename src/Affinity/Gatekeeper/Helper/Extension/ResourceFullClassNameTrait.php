<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Helper\Extension;

/**
 * 
 * Allows for a class to utilize the ResourceInterface
 * automatically.
 * 
 * @package Affinity.Gatekeeper
 * 
 */
trait ResourceFullClassNameTrait
{
    /**
     * Returns the fully qualified classname as the
     * resource identifier for authentication.
     * 
     * @return string The resource identifier.
     */
    public static function getResourceName()
    {
        return get_called_class();
    }
}