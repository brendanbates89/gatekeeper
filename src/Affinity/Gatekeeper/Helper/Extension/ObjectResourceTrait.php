<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Helper\Extension;

use Affinity\Gatekeeper\Generic\Resource\ObjectResourceProxy;

/**
 * 
 * Class Description.
 * 
 * @package Affinity.Gatekeeper
 * 
 */
trait ObjectResourceTrait
{
    public static function getResource($key = null)
    {
        return new ObjectResourceProxy(
            get_called_class(),
            self::getResourceName(), 
            $key
        );
    }
}