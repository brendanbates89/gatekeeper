<?php

/**
 * This file is part of the Affinity Development 
 * open source toolset.
 * 
 * @author Brendan Bates <brendanbates89@gmail.com>
 * @package Affinity.Gatekeeper
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */

namespace Affinity\Gatekeeper\Exception;

/**
 * 
 * UnknownResourceType exception.
 * 
 * @package Affinity.Gatekeeper
 * 
 */
class UnknownResourceTypeException extends Exception
{    
}
